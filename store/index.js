export const state = () => ({
	charactersList: [],
	page: 1,

	characterDetail: null,
});

export const mutations = {
	ADD_CHARACTERS: (state, payload) => {
		//data
			//info
			//results
		//payload
		
		if (payload.payload.searching) {
			state.charactersList = payload.data.results
		}else{
			state.charactersList = state.charactersList.concat(payload.data.results)
		}
	},
	SET_PAGE: (state, payload) => state.page = payload,

	SET_CHARACTER: (state, payload) => state.characterDetail = payload.data,

};

export const actions = {

	mapFiltersToQueryString(_, filters) {
		return Object.entries(filters)
			.filter(el => el[1] !== '')
			.map((el, index) => {
				return `${index === 0 ? '?' : '&'}${el[0]}=${el[1]}`
			}).join('')
	},

	async get_characters({ dispatch, commit }, { payload }) {

		let filters = await dispatch('mapFiltersToQueryString', {
			page: payload.page,
			name: payload.name,
			status: payload.status,
			species: payload.species,
			type: payload.type,
			gender: payload.gender,
		})

		let response = await dispatch('axios_method', {
			method: 'get',
			url: `https://rickandmortyapi.com/api/character${filters}`
		});
		if (response && [200, 201].includes(response.status)) {
			console.log('success!')
			commit('ADD_CHARACTERS', { data: response.data, payload: payload })
		} else {
			console.log('error!')
		}
	},

	async get_character({ dispatch, commit }, { payload }) {

		let response = await dispatch('axios_method', {
			method: 'get',
			url: `https://rickandmortyapi.com/api/character/${payload}`
		});
		if (response && [200, 201].includes(response.status)) {
			console.log('success!')
			commit('SET_CHARACTER', { data: response.data, payload: payload })
		} else {
			console.log('error!')
		}
	},

	async axios_method({ _ }, data = { method: 'get', url: '/', force: false, data: {} }) {
		return await this.$axios[data.method](data.url, data.data);
	},

};

export const getters = {
	getCharactersList: state => state.charactersList,
	getPage: state => state.page,

	getCharacterDetail: state => state.characterDetail,
}